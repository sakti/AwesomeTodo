-i https://pypi.org/simple
certifi==2018.8.24
chardet==3.0.4
defusedxml==0.5.0
django-allauth==0.37.1
django-bootstrap4==0.0.7
django-crispy-forms==1.7.2
django-debug-toolbar==1.9.1
django-filter==2.0.0
django==2.1
djangorestframework==3.8.2
idna==2.7
markdown==2.6.11
mysqlclient==1.3.13
oauthlib==2.1.0
python-memcached==1.59
python3-openid==3.1.0
pytz==2018.5
requests-oauthlib==1.0.0; python_version != '3.1.*'
requests==2.19.1; python_version != '3.1.*'
six==1.11.0
sqlparse==0.2.4
urllib3==1.23; python_version != '3.1.*'
