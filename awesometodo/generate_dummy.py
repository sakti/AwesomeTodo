import os
import django
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'awesometodo.settings')
django.setup()
from faker import Faker
from django.utils import timezone

fake = Faker()

if __name__ == "__main__":
    from todo.models import Task
    for i in range(200):
        task = Task()
        task.name = fake.name()
        task.description = fake.text()
        task.deadline_at = timezone.now()
        task.save()
