"""awesometodo URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf import settings
from django.urls import path, re_path, include
from rest_framework import routers
from todo import views

router = routers.DefaultRouter()
router.register("tasks", views.TaskViewSet)

# fmt: off
urlpatterns = [
    path("", views.home, name="home"), 
    path("api/", include(router.urls)), 
    path("profile/", views.user_profile), 
    path("tambah-task/", views.add_task_model_form, name="add-task-2"),
    path("admin/", admin.site.urls),
    re_path(r'^accounts/', include('allauth.urls')),
    re_path(r'^api-auth/', include('rest_framework.urls')),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        re_path(r'^__debug__/',include(debug_toolbar.urls)),
    ] + urlpatterns
