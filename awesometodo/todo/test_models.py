from datetime import timedelta
from django.test import TestCase
from .models import Task
from django.db.utils import DatabaseError
from django.utils import timezone


def create_task(name):
    task = Task(name=name)
    task.deadline_at = timezone.now() + timedelta(days=10)
    task.save()


class TaskModelTests(TestCase):
    def test_create_todo(self):
        task1 = Task(name="belajar python")
        task1.deadline_at = timezone.now() + timedelta(days=10)
        task1.save()
        self.assertEqual(task1.id, 1)

    def test_create_todo_error(self):
        task1 = Task(name="=" * 500)
        with self.assertRaises(DatabaseError):
            task1.save()

    def test_list_todo(self):
        create_task("belajar math")
        create_task("belajar fisika")
        create_task("belajar bahasa")
        tasks = Task.objects.all()
        self.assertEqual(len(tasks), 3)

        task1 = Task.objects.get(name="belajar math")
        self.assertEqual(task1.name, "belajar math")

    def test_is_late(self):
        task1 = Task(name="belajar django")
        task1.deadline_at = timezone.now() - timedelta(days=10)
        self.assertEqual(task1.is_late(), True)

    def test_is_late_but_done(self):
        task1 = Task(name="belajar django")
        task1.deadline_at = timezone.now() - timedelta(days=10)
        task1.is_done = True
        self.assertEqual(task1.is_late(), False)

    def test_is_not_late(self):
        task1 = Task(name="belajar agama")
        task1.deadline_at = timezone.now() + timedelta(days=365)
        self.assertEqual(task1.is_late(), False)
