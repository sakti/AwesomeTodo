from django.test import TestCase
from django.urls import reverse
from django.contrib.auth.models import User


class TodoViewTests(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(
            username="tester", email="tester@local.local", password="secret"
        )

    def test_home_page(self):
        home_url = reverse("home")
        login_url = reverse("account_login")
        response = self.client.get(home_url)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, login_url + "?next=" + home_url)

    def test_home_page_login(self):
        self.client.login(username="tester", password="secret")
        response = self.client.get(reverse("home"))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Awesome todo app")

    def test_create_task(self):
        self.client.login(username="tester", password="secret")
        response = self.client.post(
            reverse("add-task-2"),
            {
                "name": "Task 1",
                "description": "Task 1 description",
                "deadline_at": "2018-10-01 07:30",
            },
        )
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, reverse("home"))

